<?php

namespace App\Http\Client;

use GuzzleHttp\HandlerStack;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;

class TwitterClient
{
    private const API_BASE_URL = 'https://api.twitter.com/1.1/';
    private const TOKEN_BASE_URI = 'https://api.twitter.com/oauth2/token';

    private const USER_TIMELINE_ENDPOINT = 'statuses/user_timeline.json';

    // How many tweets per api call
    // 200 is max that Tweeter allows
    private const DEFAULT_BATCH_SIZE = 200;

    /** @var string */
    private $apiKey;

    /** @var string */
    private $apiSecretKey;

    public function __construct()
    {
        $this->setCredentials();
    }

    public function fetchTweets(string $screenName): \Generator
    {
        $client = $this->createHttpClient();

        $params = [
            'screen_name' => $screenName,
            'count' => self::DEFAULT_BATCH_SIZE,
            'max_id' => null, // max_id - keep track of the lowest ID received.
        ];

        yield from new TweetsIterator($client, self::USER_TIMELINE_ENDPOINT, $params);
    }

    protected function setCredentials(): void
    {
        $this->apiKey = env('TWITTER_API_KEY');
        $this->apiSecretKey = env('TWITTER_API_SECRET_KEY');
    }

    protected function createHttpClient(): \GuzzleHttp\Client
    {
        $oauth = $this->getOauthMiddleware();

        $stack = HandlerStack::create();
        $stack->push($oauth);

        $client = new \GuzzleHttp\Client([
            'base_uri' => self::API_BASE_URL,
            'handler' => $stack,
            'auth' => 'oauth'
        ]);

        return $client;
    }

    /**
     * Authorization client - this is used to request OAuth access tokens
     */
    protected function getOauthMiddleware(): OAuth2Middleware
    {
        $oauthClient = new \GuzzleHttp\Client(['base_uri' => self::TOKEN_BASE_URI]);

        $oauthConfig = [
            'client_id' => $this->apiKey,
            'client_secret' => $this->apiSecretKey,
        ];

        $grantType = new ClientCredentials($oauthClient, $oauthConfig);

        return new OAuth2Middleware($grantType);
    }
}
