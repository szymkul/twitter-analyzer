<?php

namespace App\Http\Client;

use Illuminate\Support\Facades\Log;
use Iterator;

/**
 * Looks for the latest tweets for a user in Twitter
 */
class TweetsIterator implements Iterator
{
    /** @var int */
    private $position = 0;

    /** @var array */
    private $tweets = [];

    /** @var string */
    private $url;

    /** @var \GuzzleHttp\Client */
    private $client;

    /** @var array */
    private $params;

    public function __construct($client, string $url, array $params)
    {
        $this->params = $params;
        $this->client = $client;
        $this->url = $url;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->tweets[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        if (!isset($this->tweets[$this->position])) {
            if ($this->position !== 0) {
                $this->prepareMaxIdForNextCall();
            }
            $this->getTweetsForIteration();
        }
        return isset($this->tweets[$this->position]);
    }

    protected function prepareMaxIdForNextCall(): void
    {
        $lastTweetKey = $this->position - 1;
        //we need to decrement tweet id by one to avoid calling for the same tweet twice
        $maxId = $this->tweets[$lastTweetKey]['id'] - 1;
        $this->params['max_id'] = $maxId;
    }

    protected function getTweetsForIteration(): void
    {
        $response = $this->client->get($this->url, ['query' => $this->params]);
        $nextTweets = \GuzzleHttp\json_decode($response->getBody(), true);

        foreach ($nextTweets as $tweet) {
            $this->tweets[] = $tweet;
        }
    }
}
