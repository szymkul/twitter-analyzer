<?php

namespace App\Http\Controllers;

use App\Services\TwitterAnalyzer;

class TwitterController extends Controller
{
    /** @var TwitterAnalyzer */
    private $analyzer;

    public function __construct(TwitterAnalyzer $analyzer)
    {
        $this->analyzer = $analyzer;
    }

    /**
     * Generate a histogram of a Twitter's user activity per hour.
     * Based on a hole period of user's activity.
     */
    public function showHistogram($screenName)
    {
        return response()->json(
            [
                'data' => $this->analyzer->generateUserHistogram($screenName),
                'error' => null
            ]
        );
    }
}
