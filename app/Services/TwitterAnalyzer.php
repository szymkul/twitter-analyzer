<?php

namespace App\Services;

use App\Http\Client\TwitterClient;
use Carbon\Carbon;

class TwitterAnalyzer
{
    /** @var TwitterClient */
    private $client;

    public function __construct(TwitterClient $client)
    {
        $this->client = $client;
    }

    /**
     * To generate a histogram we need only 'created_at' field from tweet
     */
    public function generateUserHistogram(string $screenName): array
    {
        $histogramPerHour = [];

        foreach ($this->client->fetchTweets($screenName) as $tweet) {
            $tweetDate = new Carbon($tweet['created_at']);
            $histogramPerHour = $this->addToHistogram($tweetDate, $histogramPerHour);
        }

        return $histogramPerHour;
    }

    protected function addToHistogram(Carbon $tweetDate, array $histogramPerHour): array
    {
        $tweetHour = $tweetDate->hour;

        if (isset($histogramPerHour[$tweetHour])) {
            $histogramPerHour[$tweetHour]++;
        } else {
            $histogramPerHour[$tweetHour] = 1;
        }

        return $histogramPerHour;
    }
}
