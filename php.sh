#!/usr/bin/env bash
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  -w="/app" \
  php:7.3 "$@"