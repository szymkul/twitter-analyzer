### Status
[![pipeline status](https://gitlab.com/szymkul/twitter-analyzer/badges/master/pipeline.svg)](https://gitlab.com/szymkul/twitter-analyzer/commits/master)

# Twitter analyzer (home task)

## How to run

### Docker way to run app

- have docker and docker-composer installed on your host. For more details visit [docker.com](https://www.docker.com/)
- make `*.sh` helper scripts executable by running `chmod +x composer.sh php.sh`
- run `chmod -R o+w storage/logs/`
- run `./composer.sh install`
- copy `.env.example` to `.env` and set your credentials for Twitter. If you don't have those you need to create Twitter app and generate ones. More details  [apps.twitter.com](https://apps.twitter.com/)
- from project directory run `docker-composer up -d`
- application is configured to run on port `:80`

Of course you can run app e.g. with built-in web server. From `/public`, run `php -S localhost:8080`.

## Running tests

- run `php.sh vendor/bin/phpunit` from project's root to run tests in docker container.

## TODO

1. I created repo in [gitlab.com/szymkul/twitter-analyzer](https://gitlab.com/szymkul/twitter-analyzer) instead of gitlab because I know CI in that provider. Would like to move to github and configure CI there.
2. Write more tests. For now only few exists.
3. Create documentation.
4. Use more abstraction.

## See also

- project board [gitlab.com/szymkul/twitter-analyzer/boards](https://gitlab.com/szymkul/twitter-analyzer/boards) 
- CI pipelines [gitlab.com/szymkul/twitter-analyzer/pipeline](https://gitlab.com/szymkul/twitter-analyzer/pipelines)