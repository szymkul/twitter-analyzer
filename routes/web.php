<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return 'Try /hello/:name';
});

$router->get('/hello/{name}', function ($name) {
    return "Hello $name";
});

$router->get('histogram/{username}', ['uses' => 'TwitterController@showHistogram']);

$router->get('/{any:.*}', function () {
    return response('Not Found', 404);
});