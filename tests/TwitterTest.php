<?php

/**
 * Tests are made with real credentials on real Twitter's API
 */
class TwitterTest extends TestCase
{
    public function testNotFoundGivenUser()
    {
        $userNotExistsInTwitter = '_-_not_-_Exists_-_I_hope';

        $this->get("histogram/$userNotExistsInTwitter");
        $this->assertEquals(
            '{"data":null,"error":"Given user does not exists on Twitter"}',
            $this->response->content()
        );
        $this->assertEquals(400, $this->response->getStatusCode());
    }

    public function testSuccessPath()
    {
        $realUser = 'szymkul'; //created to test real api

        $this->get("/histogram/$realUser");
        $this->assertEquals(
            '{"data":{"19":1},"error":null}',
            $this->response->content()
        );
        $this->assertEquals(200, $this->response->getStatusCode());
    }
}
