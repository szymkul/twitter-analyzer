<?php

class ApiTest extends TestCase
{
    public function testRoot()
    {
        $this->get('/');
        $this->assertEquals('Try /hello/:name', $this->response->content());
    }

    public function testHello()
    {
        $this->get('/hello/sk');
        $this->assertEquals('Hello sk', $this->response->content());
    }

    public function testNotFound()
    {
        $this->get('/not/Found/Path');
        $this->assertEquals('Not Found', $this->response->content());
        $this->assertEquals(404, $this->response->getStatusCode());
    }
}
